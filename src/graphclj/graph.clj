(ns graphclj.graph
  (:require [clojure.string :as str]))

;; Generate a graph from the lines
(defn gen-graph "Returns a hashmap contating the graph"
	[lines]
    (loop [s lines
    	   res {}]
   		(if (seq s)
   			(let [[n1 n2] (str/split (first s) #" ")
   				  node1 (Integer/parseInt n1)
   				  node2 (Integer/parseInt n2)]
   				(let [car1 (get res node1)
   					  car2 (get res node2)
   					  map1 	(if (seq (get car1 :neigh))
   					  			(update car1 :neigh conj node2)
   					  			(assoc {} :neigh #{node2}))
   					  map2 	(if (seq (get car2 :neigh))
   					  			(update car2 :neigh conj node1)
   					  			(assoc {} :neigh #{node1}))]
   					(recur (rest s) (assoc (assoc res node1 map1) node2 map2))))
   			res)))


(defn erdos-renyi-rnd "Returns a G_{n,p} random graph, also known as an Erdős-Rényi graph"
 	[n,p]
	(loop [i 0 j 0 res []]
		(if (< i n)
			(if (< j n)
				(if (not= i j)	
					(let [x (rand-int 100)]
						(if (< x (* p 100))
							(recur i (inc j) (conj res (str i " " j)))
							(recur i (inc j) res)))
					(recur i (inc j) res))
				(recur (inc i) (inc i) res))
			(let [graph (gen-graph res)]
				(loop [i 0 g graph]
					(if (< i n)
						(if (seq (get (get g i) :neigh))
							(recur (inc i) g)
							(recur (inc i) (assoc g i (assoc (get g i) :neigh #{}))))
						g))))))
