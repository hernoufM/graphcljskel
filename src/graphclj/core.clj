(ns graphclj.core
  (:require [graphclj.graph :as graph]
            [graphclj.tools :as tools]
            [graphclj.centrality :as central]
            [clojure.string :as str])
  (:gen-class))

(def graphe (atom {}))

(defn modifier-graphe "Fonction qui permet de sauvegarder les modifications"
	[g q]
	(println "\nGraphe resultat :")
	(println g "\n")
	(if q
		(do 
			(println "Il faut sauvegarder pour pouvoir le manipuler apres. Voulez vous sauvegarder votre graphe? (o/n)")
			(let [reponse (str/lower-case (str/trim (read-line)))]
				(when (= reponse "o")
					(reset! graphe g))))
		(reset! graphe g)))

(defn lecture-seq-clavier "Lecture de sequence des liens par clavier" 
	[]
	(println "\nFormat de sequence doit etre suivant:")
	(println "lien1,lien2,liens3,...,lienN")
	(println "Une lien doit avoir format suivant:\nnode1 node2")
	(println "Node doit etre une entier\n")
	(println "Entrez votre sequence :")
	(loop [s (str/split (str/trim (read-line)) #",") 
		   valide true 
		   res []]
		(if valide
			(if (seq s)
				(let [nodes (str/split (str/trim (first s)) #"\s+")]
					(if (and (= (count nodes) 2) 
					(try (do (Integer/parseInt (first nodes)) true) 
					(catch NumberFormatException e false))
					(try (do (Integer/parseInt (second nodes)) true) 
					(catch NumberFormatException e false)))
						(recur (rest s) true (conj res (str (first nodes) " " (second nodes))))
						(recur s false res)))
				(graph/gen-graph res))
			(do 
				(println "La syntaxe n'est pas correcte")
				(println "Veillez reesayer\n")
				(println "Entrez votre sequence :")
				(recur (str/split (str/trim (read-line)) #",") true [])))))

(defn lecture-seq-fichier "Lecture de sequence des liens dans fichier"
	[]
	(println "\nFichier doit contenir une lien par ligne")
	(println "Une lien doit avoir format suivant:\nnode1 node2")
	(println "Node doit etre une entier\n")
	(println "Donnez chemin absolue vers fichier (ou chemin relative au repertoire du projet) :")
	(loop [valide false]
		(let [filename (str/trim (read-line))]
			(let [s (try (tools/readfile filename)
						(catch java.io.FileNotFoundException e 
							(do 
								(println "\nFichier n'existe pas")
								(println "Donnez une autre fichier :")
								[])))]
				(if (seq s)
					(let [gr
							(loop [s s res []]
								(if (seq s)
									(let [nodes (str/split (str/trim (first s)) #"\s+")]
										(if (and (= (count nodes) 2) 
										(try (do (Integer/parseInt (first nodes)) true) 
										(catch NumberFormatException e false))
										(try (do (Integer/parseInt (second nodes)) true) 
										(catch NumberFormatException e false)))
											(recur (rest s) (conj res (str (first nodes) " " (second nodes))))
											(do 
												(println "\nFichier a une syntaxe fautive") 
												(println "Donnez une autre fichier :")
												nil)))
									(graph/gen-graph res)))]
						(if gr
							gr
							(recur false)))
					(recur false))))))

(defn lecture-aleatoire "Questionnaire pour creer graphe aleatoire"
	[]
	(let [n (do 
				(println "\nDonnez nombre de noeuds dans graph (strictement positive) :")
				(loop [valide false]
					(let [x (try (Integer/parseInt (str/trim (read-line))) 
								(catch NumberFormatException e -1))]
						(if (> 1 x)
							(do 
								(println "\nLe nombre est incorrecte")
								(println "Donnez une autre nombre :")
								(recur false))
							x))))
		  p (do 
				(println "\nDonnez une probabilite d’avoir un lien entre une paire de noeuds (entre 0.0 et 1.0 (inclus)) :")
				(loop [valide false]
					(let [x (try (Double/parseDouble (str/trim (read-line))) 
								(catch NumberFormatException e -1))]
						(if (or (> 0.0 x) (> x 1.0))
							(do 
								(println "\nLe nombre est incorrecte")
								(println "Donnez une autre probabilite :")
								(recur false))
							x))))]
		(graph/erdos-renyi-rnd n p)))


(defn menu "Menu principale du gestionnaire" 
	[]
	(println "\nD'abbord vous devez creer une graphe")
	(loop [arbreCree false sortie false]
		(if-not sortie
			(if (not arbreCree)
				(let [choix (do 
								(println "\nGestionnaire est capable de gerer les graphes non connexes")
								(println "Choisissez le methode du creation (donnez une chiffre)")
								(println "(1) Entrer la sequence des liens par clavier")
								(println "(2) Lire le fichier avec la sequence des liens")
								(println "(3) Generer une graphe aleatoire (algorithme de Erdos Renyi)")
								(println "(4) Exit")
								(str/trim (read-line)))]
					(cond
						(= choix "1") (do (modifier-graphe (lecture-seq-clavier) false) (recur true false))
						(= choix "2") (do (modifier-graphe (lecture-seq-fichier) false) (recur true false))
						(= choix "3") (do (modifier-graphe (lecture-aleatoire) false) (recur true false))
						(= choix "4") (recur arbreCree true)
						:else
						(do (println "\nChoix invalide") (recur false false))))
				(let [choix (do 
								(println "\nChoisissez l'operation dont vous souhaiter effectuer (donnez une chiffre)")
								(println "Operations possibles :")
								(println "(1) Afficher le graphe sauvegardee")
								(println "(2) Calculer et ajouter les degres pour chaque noeud dans graphe")
								(println "(3) Calculer le distance entre une noeud et le reste des noeud.")
								(println "(4) Calculer le centralité de proximité pour un noeud")
								(println "(5) Calculer et ajouter le centralité de proximité pour chaque noeud dans graphe")
								(println "(6) Calculer et ajouter les rank selon le degree pour chaque noueud dans graphe")
								(println "(7) Calculer et ajouter les rank selon le centralité de proximité pour chaque noueud dans graphe")
								(println "(8) Creer un nouveau graphe (graphe precedent sera supprimer)")
								(println "(9) Sauvegarder le graphe dans un fichier .dot")
								(println "(10) Exit")
								(str/trim (read-line)))]
					(cond
						(= choix "1") (do (println "\nVotre graphe :") (println @graphe) (recur true false))
						(= choix "2") (do (modifier-graphe (central/degrees @graphe) true) (recur true false))
						(= choix "3") (do (println "\nSi la noeud n'est pas accesible distance vaut ##Inf")
										  (println "\nDonnez le numero de noeud dans graphe :")
										  (let [n 
										  	(loop [valide false]
										  		 (let [node 
										  	 			(try (Integer/parseInt (str/trim (read-line))) 
											 			(catch NumberFormatException e -1))]
											 		(if (not (contains? @graphe node))
														(do 
															(println "\nLe noueud n'existe pas")
															(println "Donnez une autre noeud :")
															(recur false))
														node)))]
											(println "\nDistance entre " n " et autres noeuds :\n" 
												(central/distance @graphe n)))
										   (recur true false))
						(= choix "4") (do (println "\nDonnez le numero de noeud dans graph :")
										  (let [n 
										  	(loop [valide false]
										  		 (let [node 
										  	 			(try (Integer/parseInt (str/trim (read-line))) 
											 			(catch NumberFormatException e -1))]
											 		(if (not (contains? @graphe node))
														(do 
															(println "\nLe noueud n'existe pas")
															(println "Donnez une autre noued :")
															(recur false))
														node)))]
											(println "\nCentralité de proximité pour noeud " n " est : " 
												(central/closeness @graphe n)))
										   (recur true false))
						(= choix "5") (do (modifier-graphe (central/closeness-all @graphe) true) (recur true false))
						(= choix "6") (do (modifier-graphe (tools/rank-nodes (central/degrees @graphe) :degree) true) 
										(recur true false))
						(= choix "7") (do (modifier-graphe (tools/rank-nodes (central/closeness-all @graphe) :close) true) 
										(recur true false))
						(= choix "8") (recur false false)
						(= choix "9") (do (println "\nDonnez le nom du fichier (sans extention) :")
										  (let [filename (first (str/split (str/trim (read-line)) #"\s+"))]
										  	(spit (str "data/" filename ".dot") (tools/to-dot @graphe)))
										  (println "\nLe fichier est sauvegardee dans repertoire 'data' du projet")
										  (recur true false))
						(= choix "10") (recur arbreCree true)
						:else
						(do (println "\nChoix invalide") (recur true false))))))))

(defn -main
  [& args]
  (println "Bienvenue dans le gestionnaire des graphes GraphCljSkel ! ")
  (menu)
  (println "\nAu revour !"))
