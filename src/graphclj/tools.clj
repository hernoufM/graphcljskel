(ns graphclj.tools
  (:require [clojure.string :as str]))

(defn readfile "Returns a sequence from a file f" 
	[f]
    (with-open [rdr (clojure.java.io/reader f)]
            (doall (line-seq rdr))))

(defn rank-nodes "Ranks the nodes of the graph in relation to label l in accending order" 
	[g,l]
	(letfn [(calculer-rank [g, n, l]
				(let [valRank (get (get g n) l)]
			 		(loop [g g valsRankBas #{} rank 0]
			 			(if (seq g)
			 				(let [[node car] (first g)]
			 					(if (and (> valRank (+ (get car l) 0.00001)) 
			 					(not (some #(< (if (neg? (- % (get car l))) (- (get car l) %) (- % (get car l))) 0.00001) valsRankBas)))
			 						(recur (rest g) (conj valsRankBas (get car l)) (inc rank))
			 						(recur (rest g) valsRankBas rank)))
			 				rank))))]
		(loop [gr g res {}]
			(if (seq gr)
				(recur (rest gr) (assoc res (ffirst gr) (assoc (second (first gr)) :rank (calculer-rank g (ffirst gr) l))))
				res))))

(defn generate-colors [n]
    (let [step 10]
    (loop [colors {} current [255.0 160.0 122.0] c 0]
      (if (= c (inc n))
        colors
        (recur (assoc colors c (map #(/ (mod (+ step %) 255) 255) current))
               (map #(mod (+ step %) 255) current) (inc c))
      ))))


(defn to-dot "Returns a string in dot format for graph g, each node is colored in relation to its ranking"
	[g]
	(let [rankMax (loop [g g rank 0]
						(if (seq g)
							(recur (rest g) (if (< rank (get (second (first g)) :rank)) 
								(get (second (first g)) :rank) rank))
						 	rank))
		  couleurs (generate-colors rankMax)]  
		(let [chaine (loop [gr g ch ""]
						(if (seq gr)
							(recur (rest gr) 
								(str ch " " (first (first gr)) " [style=filled color=\"" 
								 (first (get couleurs (get (second (first gr)) :rank))) " "
								 (first (rest (get couleurs (get (second (first gr)) :rank)))) " "
								 (first (rest (rest (get couleurs (get (second (first gr)) :rank))))) "\"]\n"))
							ch))
			  chaine1 (loop [gr g nGr g ch ""]
			  			(if (seq gr)	
			  				(let [node1 (ffirst gr)
			  					  car (get nGr (ffirst gr))]
			  					(let [neigh (get car :neigh)]
			  						(if (seq neigh)
			  							(recur gr (assoc (assoc nGr (first neigh) 
			  								(update (get nGr (first neigh)) :neigh disj node1)) 
			  									node1 (update car :neigh disj (first neigh)))
			  										(str ch " " node1 " -- " (first neigh) "\n"))
			  							(recur (rest gr) nGr ch))))
			  				ch))]
			(str "graph g{\n" chaine chaine1 "}\n"))))
