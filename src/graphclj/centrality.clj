(ns graphclj.centrality
    (:require [graphclj.graph :as graph]
              [clojure.set :as set]))



(defn degrees "Calculates the degree centrality for each node"
	[g]
	(loop [g g res {}]
		(if (seq g)
			(let [[node car] (first g)
				   n (count (get car :neigh))]
				(recur (rest g) (assoc res node (assoc car :degree n))))
			res)))

(defn distance "Calculate the distances of one node to all the others"
	[g n]
	(loop [gr g res {}]
		(if (seq gr)
			(letfn [(calcule-distance [nSource nDest nodePasse] 
						(if (= nSource nDest)
							0.0
							(if (set/subset? (get (get g nSource) :neigh) nodePasse)
								Double/POSITIVE_INFINITY
								(loop [neigh (get (get g nSource) :neigh) res #{}]
									(if (seq neigh)
										(if (contains? nodePasse (first neigh))
											(recur (rest neigh) res)
											(recur (rest neigh) 
												(conj res (+ 1.0 
													(calcule-distance (first neigh) nDest 
														(conj nodePasse (first neigh)))))))
										(apply min res))))))]
				(if (= (ffirst gr) n)
					(recur (rest gr) (assoc res (ffirst gr) 0.0))
					(if (> (get (get (degrees g) (ffirst gr)) :degree) 0)
						(recur (rest gr) (assoc res (ffirst gr) (calcule-distance n (ffirst gr) #{n})))
						(recur (rest gr) (assoc res (ffirst gr) Double/POSITIVE_INFINITY)))))
			res)))

(defn closeness "Returns the closeness for node n in graph g"
	[g n]
	(reduce + 0.0 (map (fn [x] (if (= x 0.0) x (/ 1 x))) (vals (distance g n)))))


(defn closeness-all	"Returns the closeness for all nodes in graph g"
 	[g]
	(loop [gr g res {}]
		(if (seq gr)
			(let [[node car] (first gr)]
				(recur (rest gr) (assoc res node (assoc car :close (closeness g node)))))
			res)))